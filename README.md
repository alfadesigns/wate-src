![](http://image-store.slidesharecdn.com/1fbb745c-eb05-4630-a22c-6c36b4bfca4a-large.png)
## Wate - Source Code Sample
-----------------------------

Wate - For WhatsApp: Send Delay, Quick Tap-to-Undo Send, and Popup IM Toolbar with message-editing shortcuts

###The Idea: 
- Look at the app as one giant switchboard. 
- Observe what happens when you turn some switches on, some off.
- Create a feature set based on the results. (Settings pane in Settings.app will allow users to switch on/off
- Inspect classes at runtime
- Determine correct objects in memory to manipulate at runtime.

Features included for the following apps:
- WhatsApp.app

### A product of reverse engineering on iOS By: Suhaib Alfageeh
### Language: Objective-C & Logos preprocessor directives. 

- 1) Download WhatsApp 
- 2) Dump the binary 
- 3) Analyze assembly 
- 4) Study code-base from dumped binary headers included in WhatsApp binary 
- 5) Inject own logic into hooked classes.