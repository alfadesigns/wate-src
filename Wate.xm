
/* Copyright (C) alfaDesigns - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Suhaib Alfageeh <suhaib@alfadesigned.com>, 2014-2016
 */
#import "WateHeaders.h"
static BOOL shouldShow;
static BOOL airplaneMode = FALSE;


%group wahooks

%hook WAChatBar

BOOL locked;
BOOL frozen;
double outerLoop;
BOOL tappedd = FALSE;



-(void)viewDidLoad {

%orig;
if(Ignition)
{
	[NSNotificationCenter.defaultCenter addObserver:self selector:@selector(textViewDidChange:) name:UITextViewTextDidChangeNotification object:nil];
}
}

%new
-(void)textViewDidChange:(UITextView *)textView {
	NSLog(@"Text Changed");
	[self pop];
}
%new
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	
	
	[self pop];
	
		// Set conditional
					NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";
NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		BOOL returnKey = [[settingsDictionary objectForKey:@"returnKeySend"] boolValue];	if(returnKey && Ignition){
	if ([text isEqualToString:@"\n"])
	{
		
		[self sendButtonTapped:nil];
		return NO;
	}
	else
	{
		return YES;
	}
	
	}
}

-(void)textViewDidBeginEditing:(id)arg1 {
%orig(arg1);

//enable send return key
	WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	
	// Set conditional
					NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";
NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		BOOL returnKey = [[settingsDictionary objectForKey:@"returnKeySend"] boolValue];	if(returnKey && Ignition){
inputTextView.returnKeyType = UIReturnKeySend;
		inputTextView.enablesReturnKeyAutomatically = YES;
		
	[self pop];
	}
	NSLog(@"WateDebug : User beganediting");

	
}


-(void)textViewDidEndEditing:(id)arg1 {
%orig(arg1);

//	[self pop];
	
}


-(void)sendButtonTapped:(id)tapped {
	tappedd = FALSE;
	
	if(Ignition == TRUE){
		[self pop];
				NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";
NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		double delayInSec = [[settingsDictionary objectForKey:@"Interval"] doubleValue];
		
	 //   double sendPopupInSec = [[settingsDictionary objectForKey:@"sendPopupSec"] doubleValue];
    
    // a delay to check the content of the message first
    double sendLook = 3.0;
   
		//double outerLoop; //declare variable for for loop
		//int delayInSeconds = 5; 
		double progress = (delayInSec/10.0);
		int remaining = (int)delayInSec;
		BOOL messageSent = FALSE;
		BOOL shouldCancel = FALSE;
		
		WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	NSString *testString = [textViewForMeasuring originalText];
/* 	NSString *message = (@"Message: %@", testString);
	[JDStatusBarNotification showWithStatus:message dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
								[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
 */
if(locked == FALSE && frozen == FALSE){
	
				[BWStatusBarOverlay showLoadingWithMessage:@"Tap to Undo" animated:TRUE];
				   [BWStatusBarOverlay setActionBlock:^{
	    [BWStatusBarOverlay dismissAnimated:TRUE];
        NSLog(@"you pressed me");
       tappedd = TRUE;
       // [self cutSMS];
        	[JDStatusBarNotification showWithStatus:@"[self cutSMS]" dismissAfter:1.0 styleName:JDStatusBarStyleDefault]; 
				[self cutWAMessage];

    }];    
				[BWStatusBarOverlay setBackgroundColor:[UIColor whiteColor]];	
				
				for(outerLoop = 0.0; outerLoop<=delayInSec; outerLoop++){
				//CKMessageEntryView *_entryView = MSHookIvar<CKMessageEntryView *>(self,"_entryView");
	//NSString *copyString = [[NSString alloc] initWithFormat:@"%@",[[_entryView composition] text]];
	//NSString *newString = [copyString substringToIndex:[copyString length]-3];
	
/* 	NSLog(@"Wate String: %d", [newString length]);
								if([newString length] < 1 || newString == nil || newString == @""){ 
			//		locked = TRUE;
        //	frozen = TRUE;
					
					[BWStatusBarOverlay dismissAnimated:TRUE];
					shouldCancel = TRUE;
					break;} 	 */
						
					if(frozen == FALSE && shouldCancel == FALSE){
						
			
						dispatch_after(dispatch_time(DISPATCH_TIME_NOW, outerLoop * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ 
							//your code
															if(outerLoop == 1.0){
							[BWStatusBarOverlay showLoadingWithMessage:[NSString stringWithFormat:@"Sending in %d seconds", remaining] animated:TRUE];
							[BWStatusBarOverlay setBackgroundColor:[UIColor whiteColor]];
															}
							[BWStatusBarOverlay setProgress:progress animated:TRUE];

							if(remaining == 0 && messageSent != TRUE && shouldCancel == FALSE){ 
								
								[BWStatusBarOverlay dismissAnimated:TRUE];
							 if(tappedd == FALSE){	
			
							[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
								[JDStatusBarNotification showProgress:0.1]; 
																	%orig(); //send message
}							
							/*	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
									//[QBPopupMenu dismissAnimated:YES];
								//	[JDStatusBarNotification showWithStatus:@"Message Sent" dismissAfter:2.0 styleName:JDStatusBarStyleSuccess];   
									//[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
									
									
								});
								
								*/
							}		
						});
						if(progress>0.0 && shouldCancel == FALSE){ progress = progress - 0.1;}
						if(remaining>0 && shouldCancel == FALSE){ remaining = remaining - 1; }
						if(messageSent == TRUE){ return; }

					}		 
					
				} //end for loop
				
			} // end if locked
	else{ if(locked == TRUE){ %orig(); } }
	
		} //end if Ignition == TRUE

}

%new
-(void)sendWAMessage{

		locked = TRUE;
	frozen = TRUE;
	[self sendButtonTapped:nil];

	[self pop];
							
	locked = FALSE;
	frozen = FALSE;
	outerLoop = 100; 
	//outerLoop is greater than bound 
	//see if that works 
	[JDStatusBarNotification showWithStatus:@"Sending message without delay" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 

							}
							
%new
-(void)cutWAMessage {
	NSLog(@"WateDebug: WAChatBar cutWAMessage");
		[self pop];
		
	WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	
	//textViewForMeasuring.text = @"";
	NSString *cutMessage = inputTextView.text;
		UIPasteboard *pb = [UIPasteboard generalPasteboard];
	[pb setString:cutMessage];
	inputTextView.text = @"";	
	[JDStatusBarNotification showWithStatus:@"Message Cut to Clipboard" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	NSLog(@"Did this cut the message? - WateDebug");
	
}
%new
-(void)attachWAMessage {
[self attachMediaButtonTapped:nil];

}
%new
-(void)pasteWAMessage {
	NSLog(@"WateDebug: WAChatBar pasteWAMessage");
		[self pop];
		
			WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	
	//textViewForMeasuring.text = @"";
	//NSString *cutMessage = inputTextView.text;
		UIPasteboard *pb = [UIPasteboard generalPasteboard];
		
//:	[pb setString:cutMessage];

	NSString *pbString = pb.string;
	int length = [pbString length];
	if(length > 0){
	
	inputTextView.text = [inputTextView.text stringByAppendingString: pbString];
	
	[JDStatusBarNotification showWithStatus:@"Message Pasted to Clipboard" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	NSLog(@"Did this copy the message? - WateDebug");
	
}
	
	
	else {
			[JDStatusBarNotification showWithStatus:@"Clipboard is Empty." dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	NSLog(@"Did this copy the message? - WateDebug");
	}
	
}

%new
-(void)hideMe {
	[self pop];
	
	[[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
	// Firing twice to automate keyboard hiding
		[[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
		
	[JDStatusBarNotification showWithStatus:@"Hiding Keyboard" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	// test dismiss

}

%new
-(void)eraseWAMessage {
	NSLog(@"WateDebug: WAChatBar eraseWAMessage");
		[self pop];
		
	WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	
	//textViewForMeasuring.text = @"";
/* 	NSString *cutMessage = inputTextView.text;
		UIPasteboard *pb = [UIPasteboard generalPasteboard];
	[pb setString:cutMessage]; */
	inputTextView.text = @"";	
	[JDStatusBarNotification showWithStatus:@"Message Erased" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	NSLog(@"Did this erase the message? - WateDebug");
	
}

%new
-(void)copyWAMessage {
	NSLog(@"WateDebug: WAChatBar eraseWAMessage");
		[self pop];
	WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	
	//textViewForMeasuring.text = @"";
 	NSString *copyMessage = inputTextView.text;
		UIPasteboard *pb = [UIPasteboard generalPasteboard];
	[pb setString:copyMessage];
	//inputTextView.text = @"";	
	[JDStatusBarNotification showWithStatus:@"Message Copied to Clipboard" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
	NSLog(@"Did this copy the message? - WateDebug");
	
}

%new
-(void)pop {
				QBPopupMenuItem *item = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/send.png"] 
target:self action:@selector(sendWAMessage)];
			QBPopupMenuItem *item1 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/erase.png"] 
target:self action:@selector(eraseWAMessage)];

	QBPopupMenuItem *attachMedia = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/clip.png"] 
target:self action:@selector(attachWAMessage)];

			QBPopupMenuItem *paste = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/paste.png"] 
target:self action:@selector(pasteWAMessage)];

			QBPopupMenuItem *item2 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/copy.png"] 
target:self action:@selector(copyWAMessage)];
			QBPopupMenuItem *item3 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/cut.png"] 
target:self action:@selector(cutWAMessage)];
			QBPopupMenuItem *item4 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/hidekeyboard.png"] 
target:self action:@selector(hideMe)];
			QBPopupMenuItem *item5 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/callNow.png"] 
target:self action:nil];
			QBPopupMenuItem *item6 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/facetimeNow.png"] 
target:self action:nil];
			QBPopupMenuItem *item7 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/tower.png"] 
target:self action:nil];
			QBPopupMenuItem *item8 = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/cancel.png"] 
target:self action:nil];

			QBPlasticPopupMenu *popupMenu = [[QBPlasticPopupMenu alloc] initWithItems:@[item, item1, attachMedia, item2, paste, item3, item4, item8]];
			CGRect floatFrame = CGRectMake([UIScreen mainScreen].bounds.size.width - 54, [UIScreen mainScreen].bounds.size.height/2 - 11, 53, 33);
			UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
			
	WAInputTextView *inputTextView  = MSHookIvar<WAInputTextView *>(self,"_inputTextView");
	WATextView *textViewForMeasuring = MSHookIvar<WATextView *>(inputTextView,"_textViewForMeasuring");
	
		//	[popupMenu showInView:topViewController.view targetRect:self.frame animated:YES];
			
			[popupMenu showInView:topViewController.view targetRect:self.frame animated:YES];
			
				NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";

		NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		double popupInSec = [[settingsDictionary objectForKey:@"wateSec"] doubleValue];
		
		// PRAGMA-MARK: Popup Delay
		
		// Dismiss with preset delay
			dispatch_after(dispatch_time(DISPATCH_TIME_NOW, popupInSec * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
			[popupMenu dismissAnimated:NO];});
}

%end

%hook ChatViewController

WAChatBarManagerImpl *_chatBarManager;
NSString *copyString;
NSString *newString; // holds parsed text entry
UIImage *contactPic; // points to current recipiet rounded img

-(void)viewDidLoad{
	%orig();
	
	NSLog(@"Welcome To Chat View");
		//if(Ignition == TRUE){	

	if(Ignition != TRUE){	
		NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";

		NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		double delayInSec = [[settingsDictionary objectForKey:@"Interval"] doubleValue];
		
    double sendPopupInSec = [[settingsDictionary objectForKey:@"sendPopupSec"] doubleValue];
    
    // a delay to check the content of the message first
    double sendLook = 3.0;
   
		//double outerLoop; //declare variable for for loop
		//int delayInSeconds = 5; 
		double progress = (delayInSec/10.0);
		int remaining = (int)delayInSec;
		BOOL messageSent = FALSE;
		BOOL shouldCancel = FALSE;
		if (Ignition == TRUE) {
			QBPopupMenuItem *item = [QBPopupMenuItem 
itemWithImage:[UIImage imageWithContentsOfFile:@"/Library/Application Support/Wate/send.png"] 
target:self action:@selector(sendMessage)];

			if(locked == FALSE && frozen == FALSE){
				//Welcome Banner
				// [JDStatusBarNotification showWithStatus:@"Wate SMS" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
				// [JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
				
				[BWStatusBarOverlay showLoadingWithMessage:@"Tap to Cancel" animated:TRUE];
				   [BWStatusBarOverlay setActionBlock:^{
	    [BWStatusBarOverlay dismissAnimated:TRUE];
        NSLog(@"you pressed me");
       tappedd = TRUE;
        [self sendMessage];
        return;
    }];    
				[BWStatusBarOverlay setBackgroundColor:[UIColor whiteColor]];	
				
				for(outerLoop = 0.0; outerLoop<=delayInSec; outerLoop++){
				//CKMessageEntryView *_entryView  MSHookIvar<CKMessageEntryView *>(self,"_entryView");
	//NSString *copyString = [[NSString alloc] initWithFormat:@"%@",[[_entryView composition] text]];
	//NSString *newString = [copyString substringToIndex:[copyString length]-3];
	NSString *newString = @"test";
	NSLog(@"Wate String: %d", [newString length]);
								if([newString length] < 1 || newString == nil || newString == @""){ 
			//		locked = TRUE;
        //	frozen = TRUE;
					
					[BWStatusBarOverlay dismissAnimated:TRUE];
					shouldCancel = TRUE;
					break;} 	
						
					if(frozen == FALSE && shouldCancel == FALSE){
						
			
						dispatch_after(dispatch_time(DISPATCH_TIME_NOW, outerLoop * NSEC_PER_SEC), dispatch_get_main_queue(), ^{ 
							//your code
															if(outerLoop == 1.0){
							[BWStatusBarOverlay showLoadingWithMessage:[NSString stringWithFormat:@"Sending in %d seconds", remaining] animated:TRUE];
							[BWStatusBarOverlay setBackgroundColor:[UIColor whiteColor]];
															}
							[BWStatusBarOverlay setProgress:progress animated:TRUE];

							if(remaining == 0 && messageSent != TRUE && shouldCancel == FALSE){ 
								
								[BWStatusBarOverlay dismissAnimated:TRUE];
							 if(tappedd == FALSE){	[JDStatusBarNotification showWithStatus:@"Sending message." dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
								[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
								[JDStatusBarNotification showProgress:0.1]; 
																	//%orig(); //send message
}
								/* dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3.0 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
									//[popupMenu dismissAnimated:YES];
								//	[JDStatusBarNotification showWithStatus:@"Message Sent" dismissAfter:2.0 styleName:JDStatusBarStyleSuccess];   
									//[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 
									
									
								}); */
								
								
							}		
						});
						if(progress>0.0 && shouldCancel == FALSE){ progress = progress - 0.1;}
						if(remaining>0 && shouldCancel == FALSE){ remaining = remaining - 1; }
						if(messageSent == TRUE){ return; }

					}		 
					
				} //end for loop
				
			} // end if locked
			else{ if(locked == TRUE){ 
			//%orig(); 
			} }
		} //end if Ignition == TRUE
		
		else{ // Dele() is off.
			
			//%orig(); 
		}
	}
	else{ 
	//%orig(); 
	}


/* %new
-(void)delayStatusBar:(double)delay {
			NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";

		NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		double delayInSec = [[settingsDictionary objectForKey:@"Interval"] doubleValue];
				int remaining = (int)delayInSec;
								double progress = (delayInSec/10.0);
				for(double x = 0; x < delayInSec; x++){
							[BWStatusBarOverlay setProgress:progress animated:TRUE];
				if(progress > 0.0){progress = progress - 0.1; }
				}
} */
}
%new
-(void)sendMessage {

NSLog(@"Sent msg");

	[JDStatusBarNotification showWithStatus:@"Message Sent" dismissAfter:1.0 styleName:JDStatusBarStyleDefault];   
	[JDStatusBarNotification showActivityIndicator:TRUE indicatorStyle:UIActivityIndicatorViewStyleWhite]; 

} 
%end

%end //end group

static void PreferencesChangedCallback(CFNotificationCenterRef center, void *observer, CFStringRef name, const void *object, CFDictionaryRef userInfo) { // Action when listener detects a change
	//Perform action when switching a preference specifier
	

	//NSLog(@"Killing Settings.app");
	system("killall Settings"); //Close Settings
	system("killall WhatsApp"); //Close Settings

	
	NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //ceate pointer to peferences path

	Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];
	
returnKeySend = [preferences[PREFERENCES_ENABLED_WateReturn_KEY] boolValue];
	
	// This switch defines Ignition as the preference boolean
	//Key = [preferences[PREFERENCES_ENABLED_YTKey_KEY] boolValue];// This switch defines Ignition as the preference boolean
	//Ignition points to the Activate switch in the settings bundle
}

%ctor { //  runs first. This is what happens once the target is run. Prior to code-insertion. 
	NSLog(@"Wate:  Injecting Wate.dylib");

	[[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidFinishLaunchingNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *block) { //add listener
		NSDictionary *preferences = [[NSDictionary alloc] initWithContentsOfFile:PREFERENCES_PATH]; //Define preference path here. (PREFERENCES_PATH).
		
		if (preferences == nil) {
			preferences = @{ 

				PREFERENCES_ENABLED_YTIgnition_KEY : @(NO),
		 // comma
				
				PREFERENCES_ENABLED_WateReturn_KEY : @(NO) // comma
				//	PREFERENCES_ENABLED_YTKey_KEY : @(NO) // comma

				
			}; // Default preference value to no
			
			[preferences writeToFile:PREFERENCES_PATH atomically:YES];
		} else {

			Ignition = YES;
			returnKeySend = YES;
			//	Key = YES;



			Ignition = [preferences[PREFERENCES_ENABLED_YTIgnition_KEY] boolValue];
			//	Key = [preferences[PREFERENCES_ENABLED_YTKey_KEY] boolValue];

		 returnKeySend = [preferences[PREFERENCES_ENABLED_WateReturn_KEY] boolValue];
		 	


			
		}
		
		CFNotificationCenterAddObserver(CFNotificationCenterGetDarwinNotifyCenter(), NULL, PreferencesChangedCallback, CFSTR(PREFERENCES_CHANGED_NOTIFICATION), NULL, CFNotificationSuspensionBehaviorCoalesce);
	}];
	
		NSString *settingsPlistPath = @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist";
NSMutableDictionary *settingsDictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:settingsPlistPath];
		BOOL isOn = [[settingsDictionary objectForKey:@"Ignition"] doubleValue];
	
	if(isOn){ 
		NSLog(@"Wate: Wate Activated: Initializing");
		//%init();
		%init(wahooks);
	}
	else {
		NSLog(@"Wate: Wate Deactivated. Target will run unmodified.");
	}
	
}