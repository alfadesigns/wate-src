#import "PSSpecifier.h"
#import "PropertyFile.h"
#import <Foundation/Foundation.h>
#import <Foundation/NSDistributedNotificationCenter.h>
#import <dlfcn.h>
#import <notify.h>
#import <objc/message.h>
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "JDStatusBar/JDStatusBarNotification.h"
#import "Facade/UIView+Facade.h"
#import "BWStatusBarOverlay/BWStatusBarOverlay.h"
#import "QBPopupMenu/QBPopupMenu.h"
#import "QBPopupMenu/QBPlasticPopupMenu.h"
#import "RMDateSelectionViewController/RMActionController.h"
#import "RMDateSelectionViewController/RMDateSelectionViewController.h"
#import "RadiosPreferences.h"
#import <ShotBlocker/ShotBlocker.h>
#import "UICustomSheetClasses/UICustomActionSheet.h"
#import <UIKit/_UITextFieldRoundedRectBackgroundViewNeue.h>
//#import <SpringBoard/SBWiFiManager.h>
//#import <SpringBoard/SBTelephonyManager.h>
#import "substrate.h"
// Useful Macros:
#ifdef __IPHONE_8_0
    #define GregorianCalendar NSCalendarIdentifierGregorian
#else
    #define GregorianCalendar NSGregorianCalendar
#endif

#define WatePng @"/var/mobile/Library/Documents/WateDefault.png"
#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define IS_IPHONE ( [ [ [ UIDevice currentDevice ] model ] isEqualToString: @"iPhone" ] )
#define IS_IPHONE_5 ( IS_IPHONE && IS_WIDESCREEN )

#define PreferencesPlist @"/var/mobile/Library/Preferences/com.alfadesigns.WatePrefs.plist"
#define WateKey @"Wate"


@interface SBTelephonyManager : NSObject {}
-(BOOL)isInAirplaneMode;
-(void)updateAirplaneMode;
-(void)airplaneModeChanged;
-(void)setIsInAirplaneMode:(BOOL)airplaneMode;
+(id)sharedTelephonyManager;
@end
 
@interface SBWiFiManager : NSObject {}
-(void)setWiFiEnabled:(BOOL)enabled;
-(BOOL)wiFiEnabled;
+(id)sharedInstance;
@end
	

	
static SBTelephonyManager *telephonyManager;
static SBWiFiManager *wiFiManager;

@interface WATextView:UITextView {
	
}

-(void)copy:(id)arg1;
-(void)cut:(id)arg1;
-(id)originalAttributedText;
-(id)originalText;
-(void)setAttributedText:(id)arg1;
-(void)setFont:(id)arg1;
-(void)setText:(id)arg1;
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;

@end




@interface WAInputTextView: WATextView {
	WATextView *textViewForMeasuring;
}


@end


@interface WAChatBar: UITextView <UITextViewDelegate> {
	WAInputTextView *inputTextView;
}

- (void)sendButtonTapped:(id)tapped;
- (void)textViewDidBeginEditing:(id)arg1;
- (void)textViewDidEndEditing:(id)arg1;
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
- (void)attachMediaButtonTapped:(id)tapped;

- (void)sendWAMessage;
- (void)attachWAMessage;
- (void)pop;
- (void)hideMe;
- (void)cutWAMessage;
- (void)eraseWAMessage;
- (void)cancel;
- (void)textViewDidChange;

@end

@interface WAChatBarManagerImpl: NSObject {
	WAChatBar *_chatBar;
}


@end


/* @interface ChatViewController : UIViewController <UINavigationControllerDelegate>
{
	WAChatBarManagerImpl *_chatBarManager;
	UIView *view;
	QBPopupMenu *popupMenu;
	NSString *callNumber;
	NSArray *entities;

}

@property (nonatomic, assign, readwrite) NSString *text;
@property (nonatomic, assign, readwrite) QBPopupMenu *popupMenu;
@property (nonatomic, assign, readwrite) UIView *view;

+ (instancetype)sharedInstance;
- (id)recipients;
- (void)keyboardVisibilityWillChange;
- (void)keyboardWillHideViaGesture;
- (void)dropdownNotificationTopButtonTapped;
- (void)dropdownNotificationBottomButtonTapped;
- (void)showAlert;
- (void)layoutFacade;
- (void)updateTyping;
- (void)sendMessage;
- (void)cutSMS;
- (void)facetimeNow;
- (void)callNow;
- (void)toggleAirplaneMode;
- (void)toggleWiFi;
- (BOOL)airplaneModeEnabled;
- (BOOL)wifiEnabled;
- (id)composeDelegate;
- (void)hideKeyboard;
- (id)scrollView;
- (void)clearComposition; 
- (UINavigationController *)navigationController;
- (UICustomActionSheet *)customActionSheet;
-(void)delayStatusBar:(double)delay;

@end */
